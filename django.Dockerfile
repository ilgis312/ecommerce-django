FROM python:3.10.5-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV TZ=Europe/Moscow

RUN apt-get -qqy update && apt-get -qqy  install gcc git && pip install poetry && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /DeliveryAitrixDjango
COPY poetry.lock pyproject.toml /DeliveryAitrixDjango/
RUN poetry config virtualenvs.create false && poetry install --no-dev

COPY . /DeliveryAitrixDjango
WORKDIR /DeliveryAitrixDjango
# run entrypoint.sh

