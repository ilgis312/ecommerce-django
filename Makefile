creat-app:
	mkdir "apps/$(name)" && python manage.py startapp $(name) apps/$(name)
mg:
	python manage.py migrate
mk:
	python manage.py makemigrations
creat-user:
	python manage.py createsuperuser

coll:
	python manage.py collectstatic --noinput
git-rm:
	git rm -r --cached $(pwd)

run_task:
	celery -A DMarket worker -l info
build_save_front:
	cd frontend && docker build . --no-cache -t frontend_dm:latest -f ./frontend.Dockerfile && docker save frontend_dm:latest > frontend_dm.tar
build_save_django:
	docker build . --no-cache -t django_dm:latest -f ./django.Dockerfile && docker save django_dm:latest > django_dm.tar


#django.Dockerfile